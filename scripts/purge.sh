#!/bin/bash

echo "Logging in..."
az login \
	--service-principal \
  --username "$CLIENT_ID" \
  --password "$CLIENT_SECRET" \
  --tenant "$TENANT_ID" > /dev/null

if [[ ! -z "$SUBSCRIPTION_ID" ]]; then
  echo "Setting subscription to '$SUBSCRIPTION_ID'..."
  az account set --subscription $SUBSCRIPTION_ID
fi

IFS=','
read -a content_path <<< "$CONTENT_PATHS"

for (( n=0; n < ${#content_path[*]}; n++)); do
  echo "Purging ${content_path[n]}"
  az network front-door purge-endpoint \
    --name $FRONT_DOOR_NAME \
    --resource-group "$RESOURCE_GROUP_ID" \
    --content-paths "${content_path[n]}"
done