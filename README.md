# Bitbucket Pipelines Pipe: Azure Front Door Purge

Purge your Azure Front Door CDN endpoints using the Azure CLI.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:  


```yaml
- pipe: triadinteractive/pipe-azure-front-door-cdn-purge
  variables:
    CLIENT_ID: "<string>"
    CLIENT_SECRET: "<string>"
    TENANT_ID: "<string>"
    FRONT_DOOR_NAME: "<string>"
    RESOURCE_GROUP_ID: "<string>"
    CONTENT_PATHS: "<string>"
    SUBSCRIPTION_ID: "<string>"
```

## Variables

| Variable                | Usage                                                                                                                                                                                                                                                                                                                                                                     |
| ----------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| CLIENT_ID (\*)          | The Application (client) ID of the service account created in Azure AD (Azure AD > App registrations)                                                                                                                                                                                                                                                                     |
| CLIENT_SECRET (\*)      | The client secret generated for the AD client (Azure AD > App registrations | Certificates & secrets).                                                                                                                                                                                                                                                                    |
| TENANT_ID (\*)          | The Directory (tenant) ID found on the Azure AD app overview.                                                                                                                                                                                                                                                                                                             |
| FRONT_DOOR_NAME (\*)    | The Azure Front Door identifier.                                                                                                                                                                                                                                                                                                                                          |
| RESOURCE_GROUP_ID (\*)  | The Azure Resource Group identifier.                                                                                                                                                                                                                                                                                                                                      |
| CONTENT_PATHS (\*)      | Comma-separated path globs to purge. (ex. "/mnt/*,/dir2/*")                                                                                                                                                                                                                                                                                                               |
| SUBSCRIPTION_ID         | Subscription ID to use (optional; if omitted, default subscription is used).                                                                                                                                                                                                                                                                                              |

_(\*) = required variable._

## Details

Azure Front Door can be purged using the Azure CLI. This pipe helps simplify the process of doing this.

## Examples

### Basic example

```yaml
script:
  - pipe: triadinteractive/pipe-azure-front-door-cdn-purge
    variables:
      CLIENT_ID: "APP UUID or name(?)"
      CLIENT_SECRET: "GENERATED SECRET"
      TENANT_ID: "TENANT UUID"
      FRONT_DOOR_NAME: "NAME"
      RESOURCE_GROUP_ID: "RES. GROUP UUID"
      CONTENT_PATHS: "/dir1/*,/dir2/*"
      SUBSCRIPTION_ID: "SUBSCRIPTION UUID"
```

## Support

If you'd like help with this pipe, or you have an issue or feature request, [create a ticket](https://bitbucket.org/Triad/pipe-azure-front-door-cdn-purge/issues/new).
