FROM mcr.microsoft.com/azure-cli

WORKDIR /opt/triad/pipe

RUN az extension add --name front-door 

COPY scripts/purge.sh ./

CMD [ "/opt/triad/pipe/purge.sh" ]